# 🚨 **DEPRECATION NOTICE** 🚨

> **Important:** This repository is **deprecated** and no longer maintained.
> 
> The files for the waveform models NRHybSur3dq8, NRSur7dq4, SEOBNRv4_ROM, SEOBNRv4HM_ROM, SEOBNRv4T_surrogate, and for the remnant fits NRSur3dq8Remnant, NRSur7dq4Remnant in this repository are only compatible with LALSuite versions < 7.25.
>
> With LALSuite versions >= 7.25, please use the new version of the files available at: [https://git.ligo.org/waveforms/software/lalsuite-waveform-data](https://git.ligo.org/waveforms/software/lalsuite-waveform-data).
> 
> For LVK members, the new data is also available on CIT at `/home/lalsimulation_data` and via CVMFS at `/cvmfs/shared.storage.igwn.org/igwn/shared/auxiliary/obs_sci/cbc/waveform/lalsimulation_data`

# LALSuite-Extra

Repository containing extra data files (for ROMs/surrogates) required for use
with the LALSuite library routines.


To use this repository please setup an account on
[git.ligo.org](https://git.ligo.org) following the instructions found at
https://wiki.ligo.org/DASWG/GitLigoOrg. It is recommended to set up an ssh key
and the rest of this guide assumes you have set up an ssh key pair for
[git.ligo.org](https://git.ligo.org).

## Install Git LFS

*If you already have Git LFS installed please read this section to ensure Git
LFS is configured as you require*

Download and install [Git LFS](https://git-lfs.github.com/) for your system.
Once you have installed Git LFS you need to run the following command to
setup the command line extension. You only have to do this once, after you
install git lfs.

```bash
git lfs install --skip-smudge
```

The `--skip-smudge` option ensures that no files under LFS control are
downloaded automatically when cloning the repository or checking out new
commits containing LFS controlled files. Note that this command affects the
default download behaviour for all LFS controled repositories.


## Downloading files

Clone the repo.

```bash
git clone git@git.ligo.org:lscsoft/lalsuite-extra.git
```

To download a specific file run the following command

```bash
git lfs pull -I <file-path>
```

This will download specific file identified by `<file-path>` or you can use a
glob pattern to download a batch of files. For example:
```bash
git lfs pull -I data/lalsimulation/NRSur7dq4.h5
```

## Modifying/adding files

1. **Fork lalsuite-extra**

    To add new data to the repostiory you should create your own
    [fork](https://git.ligo.org/lscsoft/lalsuite-extra/-/forks/new) of this
    repository and then clone the repository with the command

    ```bash
    git clone git@git.ligo.org:<your-username>/lalsuite-extra.git
    ```

2.  **Add an `upstream` remote**
    ```bash
    git remote add upstream git@git.ligo.org:lscsoft/lalsuite-extra.git
    ```

3.  **Updating your local clone and remote fork**

    Before adding any new data you must make sure that your local clone is up
    to date with the
    [main repository](https://git.ligo.org/lscsoft/lalsuite-extra)

    ```bash
    git checkout master
    git fetch upstream
    git rebase upstream/master
    git push
    ```

4.  **Create a new branch**

    You must create a new branch to add data to the repository. Adding data
    directly to the `master` branch is not allowed.

    ```bash
    git checkout -b <new-branch-name>
    ```

5.  **Add new files to your local clone**

    To add new `*.h5` waveform files simply use the normal git commands

    ```bash
    # Add a new waveform
    git add <new-file-name>

    # Commit the data to the new branch
    git commit

    # Push the new branch to the remote repository
    git push --set-upstream origin <new-branch-name>
    ```

6.  **Create a merge request in the remote repository**

    Go to the [main repository](https://git.ligo.org/lscsoft/lalsuite-extra) or
    your fork, and you should see a button to start a merge request.


7.  **Merge the branch into master**

    Once the new commits have been reviewed by a second member they will be
    merged into the `master` branch of the main repository
